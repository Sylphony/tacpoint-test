# Tacpoint Assessment by Derek Zeng
This contains my results for Tacpoint's Front-end Developer assessment.

Files are located in the `src/contact/` directory.

# Notes
* The page is best viewed in modern Chrome and Firefox browsers.
* The CSS for alignment styles do not follow exactly to Zeplin's resulting pixel values and are adjusted for consistency.
* CSS are not prefixed, except for the input placeholders which Firefox requires (::moz-placeholder).
* Javascript is written in ES6 style but an ES5 one is also available for use if you need old browser support.  Simply swap the script in `index.html` if you need that.
* The page does not work in Internet Explorer due to the styles not being prefixed and lack of support of various web features.
* The page is mobile responsive.
