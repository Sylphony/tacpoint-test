/**
 * Checks the email input.
 * @param {str} input: The input.
 * @return {bool}: True if okay, false if not.
 */
function checkEmail(input) {
    // Assumptions:
    // - Accept only alphanumeric, '-', '.', '_'
    // - Accept only emails in the form of {string}@{string}.{string}
    return /^[A-Za-z0-9\-\.\_]+@[A-Za-z0-9\-\.\_]+\.[A-Za-z0-9\-\.\_]+/.test(input);

    // NOTE:
    // This is just a simple validation test for this assessment purposes.
    // In a real application, a more complex regex would be used instead
    // to cover further cases.
}

/**
 * Checks the name input.
 * @param {str} input: The input.
 * @return {bool}: True if okay, false if not.
 */
function checkName(input) {
    // Assumptions:
    // - Accept only alphanumeric, spaces, '-', '.', '\''
    // - The first character must be an alphabet character
    return /^[A-Za-z]+\s?[A-Za-z0-9\s\-\.\']*$/.test(input);

    // NOTE:
    // This is just a simple validation test for this assessment purposes.
    // In a real application, a more complex regex would be used instead
    // to cover further cases.
}

/**
 * Checks for validity of characters within the input.
 * @param {str} input: The input.
 * @return {bool}: True if valid characters, false if not.
 */
function checkValidChars(input) {
    return /[\w\d\@\!\@\#\$\%\^\&\*\(\)\{\}\[\]\\\|\-\_\+\=\;\'\:\"\<\>\,\.\?\/\~\`]+/.test(input);
}

/**
 * Validate an input field.
 * @param {Element} inputEle: The field's input element.
 * @return {bool}: True if field is valid, false if not.
 */
function validate(inputEle) {
    switch (inputEle.name) {
        case "name":
            return (checkValidChars(inputEle.value) && checkName(inputEle.value));
        case "email":
            return (checkValidChars(inputEle.value) && checkEmail(inputEle.value));
        case "message":
            return checkValidChars(inputEle.value);
        default:
            return false;
    }
}

/**
 * Update a field's status.
 * @param {Element} fieldEle: The field (wrapper) element.
 * @param {bool} [isValid = false]: If the field has valid input.
 */
function updateFieldStatus(fieldEle, isValid = false) {
    const statusClassNames = {
        success: "contact__field--success",
        error: "contact__field--error"
    };

    if (isValid) {
        fieldEle.classList.remove(statusClassNames.error);
        fieldEle.classList.add(statusClassNames.success);
        fieldEle.setAttribute("data-success", true);
    }

    else {
        fieldEle.classList.remove(statusClassNames.success);
        fieldEle.classList.add(statusClassNames.error);
        fieldEle.removeAttribute("data-success");
    }
}

/**
 * Check the form's status (if all fields passed or not).
 * @param {Element} formEle: The form element.
 * @return {bool}: True if all fields passed, false if not.
 */
function checkFormStatus(formEle) {
    for (let index = 0; index < formEle.length; index++) {
        let inputEle = formEle[index];

        if (inputEle.matches(".contact__field-input")) {
            let fieldEle = inputEle.parentNode.parentNode;

            if (!fieldEle.hasAttribute("data-success")) {
                return false;
            }
        }
    }

    return true;
}

/**
 * Set the submit button's state.
 * @param {Element} submitEle: The submit element.
 * @param {bool} [isFormValid = false]: If form has all valid fields.
 */
function setSubmitBtnState(submitEle, isFormValid = false) {
    submitEle.disabled = !isFormValid;
}

/**
 * Form main function
 */
(function(document) {
    const formEle = document.getElementById("contact__form"),
          submitEle = document.getElementById("contact__submit"),
          sentEle = document.getElementById("contact__sent");

    formEle.addEventListener("keyup", function(e) {
        // Check the text fields
        if (e.target.matches(".contact__field-input")) {
            const isValid = validate(e.target);
            updateFieldStatus(e.target.parentNode.parentNode, isValid);
        }

        // Check the form's status and set the appropriate submit button state
        const isFormValid = checkFormStatus(this);
        setSubmitBtnState(submitEle, isFormValid);
    });


    submitEle.addEventListener("click", function(e) {
        e.preventDefault();

        formEle.classList.add("contact__form--hide");
        sentEle.classList.add("contact__sent--show");
    });

})(document);

/**
 * Header main function
 */
(function(document) {
    const navToggleBtnEle = document.getElementById("header__mainNav-toggle"), 
          navMenu = document.getElementById("header__menu");

    navToggleBtnEle.addEventListener("click", function(e) {
        navMenu.classList.toggle("header__menu--show");
    });

})(document);
